<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Form;
use App\Models\User;
use App\Models\SubscriptionPlan;
use DB;
use Hash;
use Illuminate\Support\Arr;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;

class FormController extends Controller
{
     
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:form-list|form-create|form-edit|form-delete', ['only' => ['index','store']]);
         $this->middleware('permission:form-create', ['only' => ['create','store']]);
         $this->middleware('permission:form-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:form-delete', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request): View
    {
        $id = auth()->user()->id;
        if(auth()->user()->hasRole('admin')){
            $data = Form::latest()->paginate(50);
        }
        else
        {
            $data = User::join('form_subscription_plan', 'users.subscription_plan_id', '=', 'form_subscription_plan.subscription_plan_id')
            ->join('forms','form_subscription_plan.form_id','=','forms.id')
            ->where('users.id',$id)
            ->select('forms.*')
            ->latest()
            ->paginate(50);
        }
  
        return view('forms.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 50);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(): View
    {
        $subscriptionPlans = SubscriptionPlan::all();
        return view('forms.create',compact('subscriptionPlans'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request): RedirectResponse
    {
        $this->validate($request, [
            'form_name_input' => 'required',
            'plan'=>'required',
            'form_content_input' => 'required'
        ]);
    
        $input = $request->all();

        
        $input['name'] = $input['form_name_input'];
        $input['content'] = $input['form_content_input'];

        $form = Form::create($input);

        $plan = $input['plan'];
        $subscriptionPlan = SubscriptionPlan::find($plan);
        $form = Form::find($form->id);
        $form->subscriptionPlans()->attach($subscriptionPlan);

        return redirect()->route('forms.index')
                        ->with('success','Form created successfully');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id): View
    {
        $form = Form::find($id);
        $subscriptionPlan = DB::table('form_subscription_plan')->where('form_id',$form->id)->first();
        if(!empty($subscriptionPlan)) {
        $plan = SubscriptionPlan::find($subscriptionPlan->subscription_plan_id);
        $form['subscriptionPlan'] = $plan->name;
        }
        return view('forms.show',compact('form'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id): View
    {
        $form = Form::find($id);
        $form['form_name_input'] = $form->name;
        $form['form_content_input'] = $form->content;
        $subscriptionPlans = SubscriptionPlan::all();
        $subscriptionPlan = DB::table('form_subscription_plan')->where('form_id',$form->id)->first();
        if(!empty($subscriptionPlan)) {
        $plan = SubscriptionPlan::find($subscriptionPlan->subscription_plan_id);
        $form['subscriptionPlan'] = $plan->id;
        }
        $form['subscriptionPlans'] = $subscriptionPlans;
    
        return view('forms.edit',compact('form'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id): RedirectResponse
    {
        $this->validate($request, [
            'form_name_input' => 'required',
            'plan'=>'required',
            'form_content_input' => 'required'
        ]);
    
        $input = $request->all();

        $input['name'] = $input['form_name_input'];
        $input['content'] = $input['form_content_input'];
        $form = Form::find($id);
        $form->update($input);
        $plan = $input['plan'];
        $subscriptionPlan = SubscriptionPlan::find($plan);
        $form->subscriptionPlans()->sync($subscriptionPlan);
      
        return redirect()->route('forms.index')
                        ->with('success','Form updated successfully');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id): RedirectResponse
    {
        Form::find($id)->delete();
        return redirect()->route('forms.index')
                        ->with('success','Form deleted successfully');
    }
}
