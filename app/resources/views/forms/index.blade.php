@extends('layouts.app')


@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Form</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('forms.create') }}"> Create Form</a>
        </div>
    </div>
</div>


@if ($message = Session::get('success'))
<div class="alert alert-success">
  <p>{{ $message }}</p>
</div>
@endif


<table class="table table-bordered">
 <tr>
   <th>No</th>
   <th>Name</th>
   <th width="280px">Action</th>
 </tr>
 @foreach ($data as $key => $forms)
  <tr>
    <td>{{ ++$i }}</td>
    <td>{{ $forms->name }}</td>
    <td>
       <a class="btn btn-info" href="{{ route('forms.show',$forms->id) }}">Show</a>
       @if(auth()->user()->hasRole('admin'))
       <a class="btn btn-primary" href="{{ route('forms.edit',$forms->id) }}">Edit</a>
        {!! Form::open(['method' => 'DELETE','route' => ['forms.destroy', $forms->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
        {!! Form::close() !!}
       @endif
    </td>
  </tr>
 @endforeach
</table>

@if(count($data)>0)

{!! $data->render() !!}

@endif


@endsection