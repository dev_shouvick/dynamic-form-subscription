@extends('layouts.app')


@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Show Forms</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('forms.index') }}"> Back</a>
        </div>
    </div>
</div>
<br>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Name:</strong>
            {{ $form->name }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Subscription Plan:</strong>
            {{ $form->subscriptionPlan }}
        </div>
    </div>
    <br>
    <br>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Form Builder:</strong>
            <div id="build-wrap"></div>
        </div>
    </div>

</div>

<script src="{{ asset('js/vendor.min.js') }}"></script>
<script src="{{ asset('js/form-builder.min.js') }}"></script>
<script src="{{ asset('js/form-render.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
    var formData = @json($form->content);
    var options = {
      formData: formData,
      dateType: 'json'
    };
    $("#build-wrap").formRender(options);
});
</script>
@endsection