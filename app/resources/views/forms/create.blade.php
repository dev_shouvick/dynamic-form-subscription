@extends('layouts.app')


@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Create Form</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('forms.index') }}"> Back</a>
        </div>
    </div>
</div>


@if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
       @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
       @endforeach
    </ul>
  </div>
@endif



{!! Form::open(array('route' => 'forms.store','method'=>'POST')) !!}
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Name:</strong>
            {!! Form::text('form_name_input', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Select plan:</strong>
           {!! Form::select('plan', $subscriptionPlans->pluck('name', 'id'), null, ['id' => 'select_id', 'class' => 'form-control' ]) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Form Builder:</strong>
            <div id="build-wrap"></div>
           {!! Form::hidden('form_content_input', null, array('placeholder' => 'content','class' => 'form-control', 'id' => 'content_form_builder')) !!} 
        </div>
    </div>
    <br>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
{!! Form::close() !!}

<script src="{{ asset('js/vendor.min.js') }}"></script>
<script src="{{ asset('js/form-builder.min.js') }}"></script>
<script src="{{ asset('js/form-render.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
    var formBuilder = $('#build-wrap').formBuilder();
    $(window).on('scroll', function() {
    var formJsonData = formBuilder.actions.getData('json', true);
    console.log(formJsonData);
    $('#content_form_builder').val(formJsonData);
        console.log('Scrolled');
    });
});
</script>

@endsection
