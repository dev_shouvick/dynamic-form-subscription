<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SubscriptionPlan;
use DB;
use Hash;
use Illuminate\Support\Arr;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;

class SubscriptionPlanController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:plan-list|plan-create|plan-edit|plan-delete', ['only' => ['index','store']]);
         $this->middleware('permission:plan-create', ['only' => ['create','store']]);
         $this->middleware('permission:plan-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:plan-delete', ['only' => ['destroy']]);
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request): View
    {
        $data = SubscriptionPlan::latest()->paginate(50);
  
        return view('subscription_plans.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 50);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(): View
    {
        return view('subscription_plans.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request): RedirectResponse
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'price' => 'required|numeric'
        ]);
    
        $input = $request->all();
    
        $subscriptionPlan = SubscriptionPlan::create($input);

        return redirect()->route('subscription_plans.index')
                        ->with('success','Plan created successfully');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id): View
    {
        $subscriptionPlan = SubscriptionPlan::find($id);
        return view('subscription_plans.show',compact('subscriptionPlan'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id): View
    {
        $subscriptionPlan = SubscriptionPlan::find($id);
    
        return view('subscription_plans.edit',compact('subscriptionPlan'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id): RedirectResponse
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'price' => 'required|numeric'
        ]);
    
        $input = $request->all();
  
        $subscriptionPlan = SubscriptionPlan::find($id);
        $subscriptionPlan->update($input);
      
        return redirect()->route('subscription_plans.index')
                        ->with('success','Plan updated successfully');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id): RedirectResponse
    {
        SubscriptionPlan::find($id)->delete();
        return redirect()->route('subscription_plans.index')
                        ->with('success','Plan deleted successfully');
    }
}
