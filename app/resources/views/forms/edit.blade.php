@extends('layouts.app')


@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Edit Form</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('forms.index') }}"> Back</a>
        </div>
    </div>
</div>


@if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
       @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
       @endforeach
    </ul>
  </div>
@endif


{!! Form::model($form, ['method' => 'PATCH','route' => ['forms.update', $form->id]]) !!}
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Name:</strong>
            {!! Form::text('form_name_input', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Select plan:</strong>
           {!! Form::select('plan', $form->subscriptionPlans->pluck('name', 'id'), $form->subscriptionPlan, ['id' => 'select_id', 'class' => 'form-control' ]) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Form Builder:</strong>
            <div id="build-wrap"></div>
           {!! Form::hidden('form_content_input', null, array('placeholder' => 'content','class' => 'form-control', 'id' => 'content_form_builder')) !!} 
        </div>
    </div>
    <br>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
{!! Form::close() !!}

<script src="{{ asset('js/vendor.min.js') }}"></script>
<script src="{{ asset('js/form-builder.min.js') }}"></script>
<script src="{{ asset('js/form-render.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
    var formData = $('#content_form_builder').val();
    var options = {
      formData: formData,
      dateType: 'json'
    };
    var formBuilder = $('#build-wrap').formBuilder(options);
    $(window).on('scroll', function() {
    var formJsonData = formBuilder.actions.getData('json', true);
    $('#content_form_builder').val(formJsonData);
        console.log(formJsonData);
        console.log('Scrolled');
    });
});
</script>

@endsection