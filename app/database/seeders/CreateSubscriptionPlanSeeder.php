<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\SubscriptionPlan;

class CreateSubscriptionPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $basic = SubscriptionPlan::create([
            'name' => 'Basic', 
            'description' => 'Basic Subscription Plan',
            'price' => 10
        ]);

        $standard = SubscriptionPlan::create([
            'name' => 'Standard', 
            'description' => 'Standard Subscription Plan',
            'price' => 20
        ]);


        $standard = SubscriptionPlan::create([
            'name' => 'Premium', 
            'description' => 'Premium Subscription Plan',
            'price' => 30
        ]);
    }
}
