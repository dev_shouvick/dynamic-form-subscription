<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'content'
    ];

    public function subscriptionPlans() {
        return $this->belongsToMany(SubscriptionPlan::class)->withPivot('form_subscription_plan');
    }
}
