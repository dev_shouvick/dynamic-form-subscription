@extends('layouts.app')


@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Subscription Plan</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('subscription_plans.create') }}"> Create Subscription Plan</a>
        </div>
    </div>
</div>


@if ($message = Session::get('success'))
<div class="alert alert-success">
  <p>{{ $message }}</p>
</div>
@endif


<table class="table table-bordered">
 <tr>
   <th>No</th>
   <th>Name</th>
   <th>Description</th>
   <th>Price</th>
   <th width="280px">Action</th>
 </tr>
 @foreach ($data as $key => $subscription_plans)
  <tr>
    <td>{{ ++$i }}</td>
    <td>{{ $subscription_plans->name }}</td>
    <td>{{ $subscription_plans->description }}</td>
    <td> {{ $subscription_plans->price }}</td>
    <td>
       <a class="btn btn-info" href="{{ route('subscription_plans.show',$subscription_plans->id) }}">Show</a>
       <a class="btn btn-primary" href="{{ route('subscription_plans.edit',$subscription_plans->id) }}">Edit</a>
        {!! Form::open(['method' => 'DELETE','route' => ['subscription_plans.destroy', $subscription_plans->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
        {!! Form::close() !!}
    </td>
  </tr>
 @endforeach
</table>


@if(count($data)>0)

{!! $data->render() !!}

@endif


@endsection