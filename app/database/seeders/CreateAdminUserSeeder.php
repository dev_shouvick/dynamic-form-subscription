<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\SubscriptionPlan;  

class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $plan = SubscriptionPlan::pluck('id')->first();

        //for admin
        $admin = User::create([
            'name' => 'admin', 
            'email' => 'admin@example.com',
            'password' => bcrypt('123456'),
            'subscription_plan_id' => $plan
        ]);
        
        $role = Role::create(['name' => 'admin']);
         
        $permissions = Permission::pluck('id','id')->all();
       
        $role->syncPermissions($permissions);
         
        $admin->assignRole([$role->id]);

        //for user

        $user = User::create([
            'name' => 'user', 
            'email' => 'user@example.com',
            'password' => bcrypt('123456'),
            'subscription_plan_id' => $plan
        ]);
        
        $role = Role::create(['name' => 'user']);
         
        $permissions = Permission::where('name', 'form-list')->pluck('id')->first();
       
        $role->syncPermissions($permissions);
         
        $user->assignRole([$role->id]);
    }
}