<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubscriptionPlan extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'description',
        'price',
    ];

    public function forms() {
        return $this->belongsToMany(Form::class)->withPivot('form_subscription_plan');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
